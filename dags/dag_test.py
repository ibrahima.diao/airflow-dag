from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from google.cloud import storage
import yaml


# Import the function from the other file
from src.extract import load_csv_from_bucket

# Create a Google Cloud Storage client
storage_client = storage.Client()

# Define the bucket and the config file path
bucket_name = "europe-west1-event-test-a4e34be2-bucket"
config_file_path = "config/config.yaml"

# Get the bucket
bucket = storage_client.get_bucket(bucket_name)

# Get the blob and download it to a local file
blob = bucket.blob(config_file_path)
blob.download_to_filename("/tmp/config.yaml")
# Load the configuration file
with open("/tmp/config.yaml", "r") as f:
    config = yaml.safe_load(f)

dag_config = config["dag"]

default_args = {
    "owner": dag_config["default_args"]["owner"],
    "depends_on_past": dag_config["default_args"]["depends_on_past"],
    "email": [dag_config["default_args"]["email"]],
    "email_on_failure": dag_config["default_args"]["email_on_failure"],
    "email_on_retry": dag_config["default_args"]["email_on_retry"],
    "retries": dag_config["default_args"]["retries"],
    "retry_delay": dag_config["default_args"]["retry_delay_minutes"],
}

dag = DAG(
    "load_csv_from_bucket",
    description="Load CSV from GCS bucket",
    default_args=default_args,
    schedule_interval=dag_config["schedule_interval"],
    start_date=datetime.combine(dag_config["start_date"], datetime.min.time()),
    catchup=False,
)

task1 = PythonOperator(
    task_id="load_csv",
    python_callable=load_csv_from_bucket,
    dag=dag,
)

task1
