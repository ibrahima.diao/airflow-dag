import logging
import pandas as pd
from google.cloud import storage


def load_csv_from_bucket(bucket_name):
    """
    Loads multiple CSV files from the root of the bucket.

    """

    logging.info("Starting load_csv_from_bucket function...")

    # Create a Google Cloud Storage client.
    storage_client = storage.Client()

    # Extract the bucket name from the event data
    # bucket_name = "vote-for-innov-input"

    # if csv file in folder
    # folder = "/".join(bucket_path.split("/")[1:]) + "/census_"

    logging.info(f"Downloading CSV files from {bucket_name}")

    # Download all CSV files in the root of the specified bucket to a temporary directory.
    files_found = False
    dfs = []

    for blob in storage_client.list_blobs(bucket_name):
        filename = blob.name.split("/")[-1]
        if filename[-3:] == "csv":
            files_found = True
            blob.download_to_filename("/tmp/" + filename)

            # Read the CSV file and append it to the list of DataFrames
            df = pd.read_csv("/tmp/" + filename)
            dfs.append(df)

    if not files_found:
        logging.warning("No CSV files found in the specified bucket.")

    # Combine all the DataFrames into a single DataFrame
    combined_df = pd.concat(dfs, ignore_index=True)
    print(combined_df)

    logging.info("Finished load_csv_from_bucket function.")

    return combined_df
