import pandas as pd
from src.extract import load_csv_from_bucket


def test_load_csv_from_bucket(bucket_name):
    df = load_csv_from_bucket(bucket_name)
    assert type(df) == pd.DataFrame
